
/* c401.c: **********************************************************}
{* Téma: Rekurzivní implementace operací nad BVS
**                                         Vytvoøil: Petr Pøikryl, listopad 1994
**                                         Úpravy: Andrea Nìmcová, prosinec 1995
**                                                      Petr Pøikryl, duben 1996
**                                                   Petr Pøikryl, listopad 1997
**                                  Pøevod do jazyka C: Martin Tuèek, øíjen 2005
**                                         Úpravy: Bohuslav Køena, listopad 2009
**                                         Upravy: Karel Masarik, rijen 2013
**
** Implementujte rekurzivním zpùsobem operace nad binárním vyhledávacím
** stromem (BVS; v angliètinì BST - Binary Search Tree).
**
** Klíèem uzlu stromu je jeden znak (obecnì jím mùŸe být cokoliv, podle
** èeho se vyhledává). UŸiteèným (vyhledávaným) obsahem je zde integer.
** Uzly s men¹ím klíèem leŸí vlevo, uzly s vìt¹ím klíèem leŸí ve stromu
** vpravo. VyuŸijte dynamického pøidìlování pamìti.
** Rekurzivním zpùsobem implementujte následující funkce:
**
**   BSTInit ...... inicializace vyhledávacího stromu
**   BSTSearch .... vyhledávání hodnoty uzlu zadaného klíèem
**   BSTInsert .... vkládání nové hodnoty
**   BSTDelete .... zru¹ení uzlu se zadaným klíèem
**   BSTDispose ... zru¹ení celého stromu
**
** ADT BVS je reprezentován koøenovým ukazatelem stromu (typ tBSTNodePtr).
** Uzel stromu (struktura typu tBSTNode) obsahuje klíè (typu char), podle
** kterého se ve stromu vyhledává, vlastní obsah uzlu (pro jednoduchost
** typu int) a ukazatel na levý a pravý podstrom (LPtr a RPtr). Pøesnou definici typù
** naleznete v souboru c401.h.
**
** Pozor! Je tøeba správnì rozli¹ovat, kdy pouŸít dereferenèní operátor *
** (typicky pøi modifikaci) a kdy budeme pracovat pouze se samotným ukazatelem
** (napø. pøi vyhledávání). V tomto pøíkladu vám napoví prototypy funkcí.
** Pokud pracujeme s ukazatelem na ukazatel, pouŸijeme dereferenci.
**/

#include "c401.h"
int solved;

void BSTInit (tBSTNodePtr *RootPtr) {
/*   -------
** Funkce provede poèáteèní inicializaci stromu pøed jeho prvním pouŸitím.
**
** Ovìøit, zda byl jiŸ strom pøedaný pøes RootPtr inicializován, nelze,
** protoŸe pøed první inicializací má ukazatel nedefinovanou (tedy libovolnou)
** hodnotu. Programátor vyuŸívající ADT BVS tedy musí zajistit, aby inicializace
** byla volána pouze jednou, a to pøed vlastní prací s BVS. Provedení
** inicializace nad neprázdným stromem by totiŸ mohlo vést ke ztrátì pøístupu
** k dynamicky alokované pamìti (tzv. "memory leak").
**
** V¹imnìte si, Ÿe se v hlavièce objevuje typ ukazatel na ukazatel.
** Proto je tøeba pøi pøiøazení pøes RootPtr pouŸít dereferenèní operátor *.
** Ten bude pouŸit i ve funkcích BSTDelete, BSTInsert a BSTDispose.
**/



	*RootPtr = NULL;		  /* V pøípadì øe¹ení smaŸte tento øádek! */

}

int BSTSearch (tBSTNodePtr RootPtr, char K, int *Content)	{
/*  ---------
** Funkce vyhledá uzel v BVS s klíèem K.
**
** Pokud je takový nalezen, vrací funkce hodnotu TRUE a v promìnné Content se
** vrací obsah pøíslu¹ného uzlu.ŽPokud pøíslu¹ný uzel není nalezen, vrací funkce
** hodnotu FALSE a obsah promìnné Content není definován (nic do ní proto
** nepøiøazujte).
**
** Pøi vyhledávání v binárním stromu bychom typicky pouŸili cyklus ukonèený
** testem dosaŸení listu nebo nalezení uzlu s klíèem K. V tomto pøípadì ale
** problém øe¹te rekurzivním volání této funkce, pøièemŸ nedeklarujte Ÿádnou
** pomocnou funkci.
**/


if (RootPtr == NULL)
    {
        return FALSE;
    }

	if (RootPtr->Key != K)
	{
	    if (RootPtr->Key > K) // pokial je klic mensi


             return BSTSearch(RootPtr->LPtr, K, Content); //idem smerom dolava

            else

            return BSTSearch(RootPtr->RPtr, K, Content); //alebo idem smerom doprava
    }

    else
    {
                *Content = RootPtr->BSTNodeCont;
                return TRUE;


    }
}


void BSTInsert (tBSTNodePtr* RootPtr, char K, int Content)	{
/*   ---------
** VloŸí do stromu RootPtr hodnotu Content s klíèem K.
**
** Pokud jiŸ uzel se zadaným klíèem ve stromu existuje, bude obsah uzlu
** s klíèem K nahrazen novou hodnotou. Pokud bude do stromu vloŸen nový
** uzel, bude vloŸen vŸdy jako list stromu.
**
** Funkci implementujte rekurzivnì. Nedeklarujte Ÿádnou pomocnou funkci.
**
** Rekurzivní implementace je ménì efektivní, protoŸe se pøi kaŸdém
** rekurzivním zanoøení ukládá na zásobník obsah uzlu (zde integer).
** Nerekurzivní varianta by v tomto pøípadì byla efektivnìj¹í jak z hlediska
** rychlosti, tak z hlediska pamì»ových nárokù. Zde jde ale o ¹kolní
** pøíklad, na kterém si chceme ukázat eleganci rekurzivního zápisu.
**/
if ((*RootPtr) == NULL) //ak je rootptr rovny NULL, vytvorim novy prvok ktoremu naalokujem miesto
{

    tBSTNodePtr nove = malloc(sizeof(struct tBSTNode));

    if (nove == NULL)

        {return;}


        nove->Key = K;
        nove->BSTNodeCont = Content;
        (*RootPtr) = nove;
        nove->LPtr = NULL;
        nove->RPtr = NULL;



}
else
 if (*RootPtr != NULL) //kontrolujem ci rootptr nie je NULL
{
    if ((*RootPtr)->Key > K) //smerujem dolava
    {
        BSTInsert(&(*RootPtr)->LPtr, K, Content);
        //return;
    }
    else

    if ((*RootPtr)->Key < K) //smerujem doprava
    {
            BSTInsert(&(*RootPtr)->RPtr, K, Content);
           // return;
    }
    else

    if ((*RootPtr)->Key == K) //vidim ze kluc suhlasi
    {
        (*RootPtr)->BSTNodeCont = Content;
        //return;
    }
}

return;

}

void ReplaceByRightmost (tBSTNodePtr PtrReplaced, tBSTNodePtr *RootPtr) {
/*   ------------------
** Pomocná funkce pro vyhledání, pøesun a uvolnìní nejpravìj¹ího uzlu.
**
** Ukazatel PtrReplaced ukazuje na uzel, do kterého bude pøesunuta hodnota
** nejpravìj¹ího uzlu v podstromu, který je urèen ukazatelem RootPtr.
** Pøedpokládá se, Ÿe hodnota ukazatele RootPtr nebude NULL (zajistìte to
** testováním pøed volání této funkce). Tuto funkci implementujte rekurzivnì.
**
** Tato pomocná funkce bude pouŸita dále. NeŸ ji zaènete implementovat,
** pøeètìte si komentáø k funkci BSTDelete().
**/

    tBSTNodePtr nove;
    nove = NULL;
    if ((*RootPtr) != NULL)
{

    if ((*RootPtr)->RPtr != NULL)
    {
        ReplaceByRightmost(PtrReplaced, &(*RootPtr)->RPtr); //tuto sa zahrabavam
    }
    else
    {
        PtrReplaced->Key=(*RootPtr)->Key;
        PtrReplaced->BSTNodeCont = (*RootPtr)->BSTNodeCont;
        nove = (*RootPtr);

        (*RootPtr)=(*RootPtr)->LPtr;
        free(nove); //prvok na pravej strane uvolnim
        return;
    }

}
    return;

}

void BSTDelete (tBSTNodePtr *RootPtr, char K) 		{
/*   ---------
** Zru¹í uzel stromu, který obsahuje klíè K.
**
** Pokud uzel se zadaným klíèem neexistuje, nedìlá funkce nic.
** Pokud má ru¹ený uzel jen jeden podstrom, pak jej zdìdí otec ru¹eného uzlu.
** Pokud má ru¹ený uzel oba podstromy, pak je ru¹ený uzel nahrazen nejpravìj¹ím
** uzlem levého podstromu. Pozor! Nejpravìj¹í uzel nemusí být listem.
**
** Tuto funkci implementujte rekurzivnì s vyuŸitím døíve deklarované
** pomocné funkce ReplaceByRightmost.
**/


tBSTNodePtr nove; //vytvorim si pomocnu premennu
nove = NULL;
if ((*RootPtr) != NULL)
{
    if (((*RootPtr)->Key) < K) //idem smerom doprava

        BSTDelete(&((*RootPtr)->RPtr), K);

    else
    if (((*RootPtr)->Key) > K) //tuto smerom dolava

        BSTDelete(&((*RootPtr)->LPtr), K);

        else //inak
        {
            nove = (*RootPtr); //najdem uzlik
            if (nove->RPtr == NULL)
            {
                (*RootPtr) = nove->LPtr;
                free(nove); //uvolnim
            }
            else
            if (nove->LPtr == NULL)
            {
                (*RootPtr) = nove->RPtr;
                free(nove); //uvolnim
            }

            else

            ReplaceByRightmost((*RootPtr),&(*RootPtr)->LPtr); //tuto ho nahradim podla smeru vetvy
        }
}



}

void BSTDispose (tBSTNodePtr *RootPtr) {
/*   ----------
** Zru¹í celý binární vyhledávací strom a korektnì uvolní pamì».
**
** Po zru¹ení se bude BVS nacházet ve stejném stavu, jako se nacházel po
** inicializaci. Tuto funkci implementujte rekurzivnì bez deklarování pomocné
** funkce.
**/


if (*RootPtr != NULL)
{
    BSTDispose(&(*RootPtr)->LPtr); //idem smerom vlavo
    BSTDispose(&(*RootPtr)->RPtr); //idem smerom vpravo ak prejdem lavu stranu

    free(*RootPtr); //korektne uvolnim pamat
    (*RootPtr) = NULL; // uvediem do noveho stavu


}

}

/* konec c401.c */

