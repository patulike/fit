--Patricia Jamriskova, xjamri01
--Jakub Kriz, xkrizj06

DROP TABLE nemoc CASCADE CONSTRAINTS;
DROP TABLE lekar CASCADE CONSTRAINTS;
DROP TABLE lecba CASCADE CONSTRAINTS;
DROP TABLE leky CASCADE CONSTRAINTS;
DROP TABLE zvire CASCADE CONSTRAINTS;
DROP TABLE majitel CASCADE CONSTRAINTS;
DROP TABLE sestra CASCADE CONSTRAINTS;
DROP TABLE druh CASCADE CONSTRAINTS;
DROP TABLE davkovani CASCADE CONSTRAINTS;
DROP TABLE naordinovano CASCADE CONSTRAINTS;


CREATE TABLE nemoc(
  zkratka VARCHAR(50),
  CONSTRAINT PK_nemoc PRIMARY KEY (zkratka) ENABLE
);

CREATE TABLE lekar (
	osobni_cislo INTEGER,
	jmeno VARCHAR(50),
	prijmeni VARCHAR(50),
	titul VARCHAR(50),
	adresa VARCHAR(255),
  cislo_uctu CHAR(11) NOT NULL,
	hodinova_mzda INTEGER ,
	CONSTRAINT PK_lekar PRIMARY KEY (osobni_cislo) ENABLE
);


CREATE TABLE lecba (
	kod_lecby INTEGER,
	datum_zahajeni DATE,
	stav VARCHAR(255),
	cena INTEGER NOT NULL,
	CONSTRAINT PK_lecba PRIMARY KEY (kod_lecby) ENABLE
);


CREATE TABLE leky (
	registracni_cislo INTEGER,
	typ_leku VARCHAR(50),
	ucinna_latka VARCHAR(50),
	kontraindikace VARCHAR(255),
  CONSTRAINT PK_leky PRIMARY KEY (registracni_cislo) ENABLE
);


 CREATE TABLE zvire (
 id_zvire INTEGER NOT NULL,
 jmeno VARCHAR(50),
 datum_narozeni DATE,
 datum_posledni_kontroly DATE,
 informace_jednotlivych_leceb VARCHAR(255),
 id_majitel INTEGER,
 id_nazev VARCHAR(50),
 CONSTRAINT PK_zvire PRIMARY KEY (id_zvire) ENABLE
 ); 
 
 
 CREATE TABLE  majitel (
 id_majitel INTEGER NOT NULL,
 jmeno VARCHAR(50) NOT NULL,
 prijmeni VARCHAR(50) NOT NULL,
 adresa VARCHAR(255),
 CONSTRAINT PK_majitel PRIMARY KEY (id_majitel) ENABLE 
 );
 
 
 CREATE TABLE sestra (
 id_sestra INTEGER,
 jmeno VARCHAR(50),
 prijmeni VARCHAR(50),
 titul CHAR(50),
 adresa VARCHAR(255),
 cislo_uctu CHAR(11) NOT NULL,
 hodinova_mzda INTEGER NOT NULL,
 CONSTRAINT PK_sestra PRIMARY KEY (id_sestra) ENABLE
 );
 
 
 CREATE TABLE druh (
 id_nazev VARCHAR(50),
 CONSTRAINT PK_druh PRIMARY KEY (id_nazev) ENABLE
 );
 
CREATE TABLE davkovani(
id_davkovani VARCHAR(50) NOT NULL,
mnozstvi VARCHAR(50) NOT NULL,
id_leky INTEGER NOT NULL,
id_druh VARCHAR(50) NOT NULL,
id_nemoc VARCHAR (50) NOT NULL,
 CONSTRAINT PK_davkovani PRIMARY KEY (id_davkovani) ENABLE
);

CREATE TABLE naordinovano(
id_lecby INTEGER,
id_leky  INTEGER,
davkovani VARCHAR(50),
doba_podavani VARCHAR(50)
);


INSERT INTO lekar (osobni_cislo, jmeno, prijmeni, titul, adresa, cislo_uctu, hodinova_mzda) VALUES ('19041', 'Radoslav', 'Parnica', 'Berkova 94, 602 00, Brno', 'MVDr.', '14218/0400', '250');
INSERT INTO lekar (osobni_cislo, jmeno, prijmeni, titul, adresa, cislo_uctu, hodinova_mzda) VALUES ('14720', 'Marie', 'Stanicka', 'Stanislava Manharda 8, 796 01, Prostejov', 'MVDr.', '12904/1111', '325');
INSERT INTO lekar (osobni_cislo, jmeno, prijmeni, titul, adresa, cislo_uctu, hodinova_mzda) VALUES ('21284', 'Marek', 'Sponar', 'Jilova 14, 760 01, Zlin', '', '19040/0800', '80');


INSERT INTO lecba (kod_lecby, datum_zahajeni, stav, cena) VALUES ('11', TO_DATE ('12.10.2013', 'dd.mm.yyyy'), 'zahajeno', '2875');
INSERT INTO lecba (kod_lecby, datum_zahajeni, stav, cena) VALUES ('40', TO_DATE ('04.11.2013', 'dd.mm.yyyy'), 'zhorsuje se', '1420');
INSERT INTO lecba (kod_lecby, datum_zahajeni, stav, cena) VALUES ('05', TO_DATE ('26.02.2014', 'dd.mm.yyyy'), 'vyleceno', '490');

INSERT INTO leky (registracni_cislo, typ_leku, ucinna_latka, kontraindikace) VALUES ('9705499', 'inaktivovana vakcina', 'Borrelia burgdorferi', 'hypersenzivita');
INSERT INTO leky (registracni_cislo, typ_leku, ucinna_latka, kontraindikace) VALUES ('5190938', 'dezinfekcia', 'Elektryzovana voda', 'zadne');
INSERT INTO leky (registracni_cislo, typ_leku, ucinna_latka, kontraindikace) VALUES ('67106439', 'antibiotikum', 'Gentamycin', 'u kocek nefrotoxicita');


INSERT INTO zvire (id_zvire, jmeno, datum_narozeni, datum_posledni_kontroly, informace_jednotlivych_leceb, id_majitel, id_nazev) VALUES ('457', 'Buddy', TO_DATE('15.05.2006', 'dd.mm.yyyy'), TO_DATE ('20.11.2013', 'dd.mm.yyyy'), 'vylecena skrtavka, vylecen zapal leveho ucha, operace kolenniho kloubu', '1426', 'zlaty retrivr' );
INSERT INTO zvire (id_zvire, jmeno, datum_narozeni, datum_posledni_kontroly, informace_jednotlivych_leceb, id_majitel, id_nazev) VALUES ('1672', 'Fufik', TO_DATE('09.09.2012', 'dd.mm.yyyy'),TO_DATE ('24.03.2014', 'dd.mm.yyyy'), 'operace leve zadni tlapky po zasahu pozarem, opakovany prujem-lecba dietou, mirna nadvaha-majitel se pokousi o upravu stravy a aktivnejsi pohyb pro zvire', '1725', 'perzska kocka');
INSERT INTO zvire (id_zvire, jmeno, datum_narozeni, datum_posledni_kontroly, informace_jednotlivych_leceb, id_majitel, id_nazev) VALUES ('2001', 'Sisi', TO_DATE ('14.02.2014', 'dd.mm.yyyy'),TO_DATE ('20.03.2014', 'dd.mm.yyyy'), 'odvrhnuti od matky-slaba podvyziva, zanet ocnich spojivek', '1938', 'morce domaci');
                                                                                                                                                                                              
INSERT INTO majitel (id_majitel, jmeno, prijmeni, adresa) VALUES ('1426', 'Monika', 'Benova', 'Kounicova 21, 602 00, Brno');
INSERT INTO majitel (id_majitel, jmeno, prijmeni, adresa) VALUES ('1725', 'Tomas', 'Cerveny', 'Rybizova 2, 602 00, Brno');
INSERT INTO majitel (id_majitel, jmeno, prijmeni, adresa) VALUES ('1938', 'Pavel', 'Vojtech', 'Stefanikova 45, 602 00 Brno');

INSERT INTO sestra (id_sestra, jmeno, prijmeni, titul, adresa, cislo_uctu, hodinova_mzda) VALUES ('22013', 'Karolina', 'Blechova', 'MVDr.', 'Bartosova 13, 695 01, Hodonin', '123789/0000', '170');
INSERT INTO sestra (id_sestra, jmeno, prijmeni, titul, adresa, cislo_uctu, hodinova_mzda) VALUES ('57607', 'Aneta', 'Dvorakova', 'Mgr.', 'Bratislavska 512, 691 45, Podivin', '147707/0400', '149');
INSERT INTO sestra (id_sestra, jmeno, prijmeni, titul, adresa, cislo_uctu, hodinova_mzda) VALUES ('54394', 'Karol', 'Freund', 'MVDr.', 'Uvoz 35, 602 00, Brno', '95391/0800', '220');
 
INSERT INTO druh (id_nazev) VALUES ('zlaty retrivr');
INSERT INTO druh (id_nazev) VALUES ('perzska kocka');
INSERT INTO druh (id_nazev) VALUES ('morce domaci');



INSERT INTO davkovani (id_davkovani, mnozstvi, id_leky, id_druh, id_nemoc) VALUES ('2ml na 1kg vahy', '1x za den', '9705499', 'zlaty retrivr', 'borr');
INSERT INTO davkovani (id_davkovani, mnozstvi, id_leky, id_druh, id_nemoc) VALUES ('18ml na 1kg vahy', '3x za den', '5190938', 'morce domaci', 'subcell');
INSERT INTO davkovani (id_davkovani, mnozstvi, id_leky, id_druh, id_nemoc) VALUES ('4ml na 1kg vahy', '2x za den', '9705499', 'perzska kocka', 'borr');
 
INSERT INTO nemoc (zkratka) VALUES ('borr'); --borelioza
INSERT INTO nemoc (zkratka) VALUES ('konjuktiv'); --zapal ocnych spojiviek
INSERT INTO nemoc (zkratka) VALUES ('subcell'); --k dezinfekcii



INSERT INTO naordinovano(id_leky, id_lecby) VALUES ('9705499', '11') ;
INSERT INTO naordinovano(id_leky, id_lecby) VALUES ('5190938', '40')  ;
INSERT INTO naordinovano(id_leky, id_lecby) VALUES ('67106439', '05');
             
ALTER TABLE naordinovano                            
ADD CONSTRAINT naordinovano_leky_FK1 FOREIGN KEY ( id_leky ) REFERENCES leky ( registracni_cislo ) ENABLE
ADD CONSTRAINT naordinovano_lecba_FK2 FOREIGN KEY ( id_lecby ) REFERENCES lecba ( kod_lecby ) ENABLE  ;
                            
ALTER TABLE zvire                            
ADD CONSTRAINT zvire_majitel_FK1 FOREIGN KEY ( id_majitel ) REFERENCES majitel ( id_majitel ) ENABLE
ADD CONSTRAINT zvire_druh_FK2 FOREIGN KEY ( id_nazev ) REFERENCES druh ( id_nazev ) ENABLE  ;


ALTER TABLE davkovani
ADD CONSTRAINT davkovani_leky_FK1 FOREIGN KEY ( id_leky ) REFERENCES leky ( registracni_cislo ) ENABLE
ADD CONSTRAINT davkovani_druh_FK2 FOREIGN KEY ( id_druh ) REFERENCES druh ( id_nazev ) ENABLE
ADD CONSTRAINT davkovani_nemoc_FK3 FOREIGN KEY ( id_nemoc ) REFERENCES nemoc ( zkratka ) ENABLE ;

COMMIT;
