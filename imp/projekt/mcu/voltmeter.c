/*******************************************************************************
   thermometer.c: Funkce pro mereni teploty
   Copyright (C) 2007 Brno University of Technology,
                      Faculty of Information Technology
   Author(s): Patricia Jamriskova <xjamri01 AT fit.vutbr.cz>

   LICENSE TERMS

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:
   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in
      the documentation and/or other materials provided with the
      distribution.
   3. All advertising materials mentioning features or use of this software
      or firmware must display the following acknowledgement:

        This product includes software developed by the University of
        Technology, Faculty of Information Technology, Brno and its
        contributors.

   4. Neither the name of the Company nor the names of its contributors
      may be used to endorse or promote products derived from this
      software without specific prior written permission.

   This software or firmware is provided ``as is'', and any express or implied
   warranties, including, but not limited to, the implied warranties of
   merchantability and fitness for a particular purpose are disclaimed.
   In no event shall the company or contributors be liable for any
   direct, indirect, incidental, special, exemplary, or consequential
   damages (including, but not limited to, procurement of substitute
   goods or services; loss of use, data, or profits; or business
   interruption) however caused and on any theory of liability, whether
   in contract, strict liability, or tort (including negligence or
   otherwise) arising in any way out of the use of this software, even
   if advised of the possibility of such damage.

   $Id$


*******************************************************************************/

#include <msp430x16x.h>
#include <stdlib.h>
#include <fitkitlib.h>
#include <stdio.h>
#include <lcd/display.h>
#include "vga_block.h"
//


#define REFERENCE 5
#define MAX 4096
#define STEP (int)REFERENCE/MAX

int values_osc[65];

/*******************************************************************************
 *  Zjisteni hodnoty kanalu 10 AD prevodniku MCU (teplotni sensor)
 *  Bere se prumer z osmi hodnot
*******************************************************************************/
unsigned long voltmeter_measure(void) {
   unsigned long ReturnValue;

   ADC12CTL0 = ADC12ON | SHT0_15 | MSH | REFON;  // ADC on, int. ref. on (1,5 V),
                                                 // multiple sample & conversion
   ADC12CTL1 = ADC12SSEL_2 | ADC12DIV_7 | CSTARTADD_0 | CONSEQ_1 | SHP;

   ADC12MCTL0 = SREF_1 | INCH_0;                 // int. ref., channel 10
   ADC12MCTL1 = SREF_1 | INCH_0;                 // int. ref., channel 10
   ADC12MCTL2 = SREF_1 | INCH_0;                 // int. ref., channel 10
   ADC12MCTL3 = SREF_1 | INCH_0;                 // int. ref., channel 10
   ADC12MCTL4 = SREF_1 | INCH_0;                 // int. ref., channel 10
   ADC12MCTL5 = SREF_1 | INCH_0;                 // int. ref., channel 10
   ADC12MCTL6 = SREF_1 | INCH_0;                 // int. ref., channel 10
   ADC12MCTL7 = EOS | SREF_1 | INCH_0;           // int. ref., channel 10, last seg.

   ADC12CTL0 |= ENC;                              // povoleni prevodu
   ADC12CTL0 |= ADC12SC;                          // sample & convert

   while (ADC12CTL0 & ADC12SC);                   // cekani na prevod

   ADC12CTL0 &= ~ENC;

   ReturnValue = ADC12MEM0;                       // secteni hodnot
   ReturnValue += ADC12MEM1;
   ReturnValue += ADC12MEM2;
   ReturnValue += ADC12MEM3;
   ReturnValue += ADC12MEM4;
   ReturnValue += ADC12MEM5;
   ReturnValue += ADC12MEM6;
   ReturnValue += ADC12MEM7;

   ReturnValue >>= 3;                             // zprumerovani hodnot

   return ReturnValue;
}

/*******************************************************************************
 *  Prevod namerene hodnoty na teplotu ve stupnich Celsia
*******************************************************************************/
int voltmeter_getV(void) {
   unsigned long value = voltmeter_measure();

   int num = 100 * value * 5 / 4095;

   return num;
}


/*******************************************************************************
 *  Inicializace generatoru nahodnych cisel
*******************************************************************************/
void voltmeter_init_rand(void) {
   srand(voltmeter_measure());
}





void print_user_help(void)
{
    term_send_str_crlf("Zobrazeni teploty cidla v MCU na displeji.");
}

/*******************************************************************************
 * Dekodovani a vykonani uzivatelskych prikazu
*******************************************************************************/
unsigned char decode_user_cmd(char *cmd_ucase, char *cmd)
{
   return (CMD_UNKNOWN);
}

/*******************************************************************************
 * Inicializace periferii/komponent po naprogramovani FPGA
*******************************************************************************/
void fpga_initialized()
{
   LCD_init();
   LCD_write_string("Thermometer ...");
}



/*******************************************************************************
 * Hlavni funkce
*******************************************************************************/
int main(void)
{
      short counter = 0;                           // udaj o teplote
      char result[20];                      // textovy retezec zobrazeny na displeji

      initialize_hardware();

      set_led_d6(1);                       // rozsviceni D6
      set_led_d5(1);                       // rozsviceni D5



      while (1) {

            delay_ms(50);  //zpozdeni 1ms

            //counter++;
            //if (counter == 500)
            //{
            flip_led_d6(); //invertovat LED
            LCD_clear ();
            int temp = voltmeter_getV();                  // zjisteni aktualni teploty
            sprintf (result, "%03d", temp);
            result[3] = result [2];
            result[2] = result [1];
            result[4] = 'V';
            result[1] = '.';
            result[5] = 0;
            LCD_write_string(result);        // zobrazeni teploty
            term_send_str_crlf(result);

            VGA_SetPixelXY (78, 0, 255);
            VGA_SetPixelXY (79, 1, 255);
            VGA_SetPixelXY (79, 2, 255);
            VGA_SetPixelXY (79, 3, 255);
            VGA_SetPixelXY (78, 4, 255);
            VGA_SetPixelXY (77, 1, 255);
            VGA_SetPixelXY (77, 2, 255);
            VGA_SetPixelXY (77, 3, 255);

            counter = 0;
            int idx = 0;
            int i = 0;
            for (i = 0; i < 64; ++i) {
                  idx = (int)(11 * values_osc[i] / 100);
                  VGA_SetPixelXY (i, idx, 200);

                  values_osc[i] = values_osc[i + 1];
                  idx = (int)(11 * values_osc[i] / 100);
                  VGA_SetPixelXY (i, idx, 1);
                  //++i;
            }

            idx = (int) (11 * values_osc[i - 1] / 100);
            VGA_SetPixelXY (i, idx, 200);

            values_osc[i] = temp;
            idx = (int)(11 * values_osc[i] / 100);
            VGA_SetPixelXY (i, idx, 1);
            //}

            terminal_idle();  // obsluha terminalu
      }

}
