-- fsm.vhd: Finite State Machine
-- Author(s): Patr�cia Jamri�kov�, xjamri01@fit.vutbr.cz
--
library ieee;
use ieee.std_logic_1164.all;
-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity fsm is
port(
   CLK         : in  std_logic;
   RESET       : in  std_logic;

   -- Input signals
   KEY         : in  std_logic_vector(15 downto 0);
   CNT_OF      : in  std_logic;

   -- Output signals
   FSM_CNT_CE  : out std_logic;
   FSM_MX_MEM  : out std_logic;
   FSM_MX_LCD  : out std_logic;
   FSM_LCD_WR  : out std_logic;
   FSM_LCD_CLR : out std_logic
);
end entity fsm;

-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of fsm is
   type t_state is (IDLE, CMP1, CMP2, CMP3, CMP4, CMP5, CMP6, CMP7, CMP8, CMP9, CMP10, CMP11, CMP5B, CMP6B, CMP7B, CMP8B, CMP9B, PRINT_MESSAGE_OK, PRINT_MESSAGE_ERR, FAIL, FINISH);
   signal present_state, next_state : t_state;

begin
-- -------------------------------------------------------
sync_logic : process(RESET, CLK)
begin
   if (RESET = '1') then
      present_state <= IDLE;
   elsif (CLK'event AND CLK = '1') then
      present_state <= next_state;
   end if;
end process sync_logic;

-- -------------------------------------------------------
next_state_logic : process(present_state, KEY, CNT_OF)
begin
   case (present_state) is
   -- - - - - - - - - - - - - - - - - - - - - - -
   when IDLE =>
      next_state <= CMP1;
   -- - - - - - - - - - - - - - - - - - - - - - -
	when CMP1 =>
	next_state<=CMP1;
	if (KEY(7)='1') then 
	next_state <=CMP2;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	-----------------------------------------------
	when CMP2 =>
	next_state<=CMP2;
	if (KEY(8)='1') then 
	next_state <=CMP3;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	-----------------------------------------------
	when CMP3 =>
	next_state<=CMP3;
	if (KEY(2)='1') then 
	next_state <=CMP4;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	------------------------------------------------
	-----------zmeny--------------------------------
	------------------------------------------------
	when CMP4 =>
	next_state<=CMP4;
	if (KEY(0)='1') then 
	next_state <=CMP5;
	elsif (KEY(2)='1') then 
	next_state <=CMP5B;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	-------------------------------------------------
	when CMP5 =>
	next_state<=CMP5;
	if (KEY(3)='1') then 
	next_state <=CMP6;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	------------------------------------------------
	when CMP5B =>
	next_state<=CMP5B;
	if (KEY(2)='1') then 
	next_state <=CMP6B;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	---------------------------------------------
	when CMP6 =>
	next_state<=CMP6;
	if (KEY(8)='1') then 
	next_state <=CMP7;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	---------------------------------------------
	when CMP6B =>
	next_state<=CMP6B;
	if (KEY(2)='1') then 
	next_state <=CMP7B;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	---------------------------------------------
	when CMP7 =>
	next_state<=CMP7;
	if (KEY(2)='1') then 
	next_state <=CMP8;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	---------------------------------------------
	when CMP7B =>
	next_state<=CMP7B;
	if (KEY(9)='1') then 
	next_state <=CMP8B;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	---------------------------------------------
	when CMP8 =>
	next_state<=CMP8;
	if (KEY(3)='1') then 
	next_state <=CMP9;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	------------------------------------------------
	when CMP8B =>
	next_state<=CMP8B;
	if (KEY(4)='1') then 
	next_state <=CMP9B;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	----------------------------------------------
	when CMP9 =>
	next_state<=CMP9;
	if (KEY(8)='1') then 
	next_state <=CMP10;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	---------------------------------------------
	when CMP9B =>
	next_state<=CMP9B;
	if (KEY(2)='1') then 
	next_state <=CMP10;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_ERR;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	---------------------------------------------
	when CMP10=>
	next_state<=CMP10;
	if (KEY(9)='1') then 
	next_state <=CMP11;
	elsif(KEY(15)='1') then
	next_state <=PRINT_MESSAGE_OK;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	---------------------------------------------
	when CMP11 =>
	next_state <= CMP11;
	if (KEY(15)='1') then 
	next_state <=PRINT_MESSAGE_OK;
	elsif(KEY(15 downto 0)/="0000000000000000") then
	next_state <=FAIL;
	end if;
	---------------------------------------------
	when FAIL =>
	next_state <= FAIL;
	if (KEY(15)='1') then
	next_state <= PRINT_MESSAGE_ERR;
	end if;
	---------------------------------------------
   when PRINT_MESSAGE_OK =>
      next_state <= PRINT_MESSAGE_OK;
      if (CNT_OF = '1') then
         next_state <= FINISH;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
	when PRINT_MESSAGE_ERR =>
	next_state <= PRINT_MESSAGE_ERR;
	if (CNT_OF = '1') then
         next_state <= FINISH;
      end if;
	-----------------------------------------------
   when FINISH =>
      next_state <= FINISH;
      if (KEY(15) = '1') then
         next_state <= CMP1; 
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when others =>
      next_state <= IDLE;
   end case;
end process next_state_logic;

-- -------------------------------------------------------
output_logic : process(present_state, KEY)
begin
   FSM_CNT_CE     <= '0';
   FSM_MX_MEM     <= '0';
   FSM_MX_LCD     <= '0';
   FSM_LCD_WR     <= '0';
   FSM_LCD_CLR    <= '0';

   case (present_state) is
   -- - - - - - - - - - - - - - - - - - - - - - -
	when PRINT_MESSAGE_ERR =>
		FSM_MX_MEM		<= '0';
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';
   -- - - - - - - - - - - - - - - - - - - - - - -
	when PRINT_MESSAGE_OK =>
		FSM_MX_MEM		<= '1';
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';
	------------------------------------------------
   when FINISH =>
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when others =>
	if (KEY(14 downto 0)/="000000000000000") then
	FSM_LCD_WR <='1';
	end if;
	if (KEY(15)='1') then
	FSM_LCD_CLR <='1';
	end if;
	---------------------------------------------
   end case;
end process output_logic;

end architecture behavioral;

