--Vypracovala: Patricia Jamriskova, xjamri01
--datum: 25.11.2013

library IEEE;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

entity ledc8x8 is
	port(
		SMCLK: in std_logic;
		ROW: out std_logic_vector (0 to 7);
		LED: out std_logic_vector (0 to 7);
		RESET: in std_logic		
	);
end entity ledc8x8;


architecture chovanie of ledc8x8 is
	signal ctrl_cnt: STD_LOGIC_VECTOR (21 downto 0);
	signal ce: STD_LOGIC;
	signal row_cnt: STD_LOGIC_VECTOR (0 to 7) := "10000000";
	signal switch: STD_LOGIC;
	signal triotazniky: STD_LOGIC_VECTOR (0 to 7);
begin

deleniSig: process (RESET, SMCLK) --Vydelim signal ..
begin
	if RESET = '1' then
		ctrl_cnt <= (others => '0');
	elsif SMCLK'event and SMCLK = '1' then
		ctrl_cnt <= ctrl_cnt + 1;
		if ctrl_cnt(7 downto 0) = "11111111" then 
			ce <= '1';
		else ce <= '0';
		end if;
	end if;
	
	switch <= ctrl_cnt(21);
end process;

aktRadku: process (RESET, SMCLK) --tuto aktivujem riadky
begin
	if RESET = '1'  then
		row_cnt <= "10000000";
	
	elsif SMCLK'event and SMCLK = '1' then --tuto rotuju riadky jeden za druhym
		if ce = '1' then
			case row_cnt is         
				when "00000001" => row_cnt <= "10000000";
				when "00000010" => row_cnt <= "00000001";
				when "00000100" => row_cnt <= "00000010";
				when "00001000" => row_cnt <= "00000100";
				when "00010000" => row_cnt <= "00001000";
				when "00100000" => row_cnt <= "00010000";
				when "01000000" => row_cnt <= "00100000";
				when "10000000" => row_cnt <= "01000000";	
				when others => null;
			end case;
		end if;
	end if;		
end process;

blikani: process (row_cnt)
begin
	
	case row_cnt is         
		when "10000000" => triotazniky <= "00001111"; --****
		when "01000000" => triotazniky <= "01100000"; --*  *****
		when "00100000" => triotazniky <= "00001110"; --****   *
		when "00010000" => triotazniky <= "01111110"; --*      *
		when "00001000" => triotazniky <= "01111110"; --*      *
		when "00000100" => triotazniky <= "01111110"; --*      *
		when "00000010" => triotazniky <= "11110110"; --    *  *
		when "00000001" => triotazniky <= "11111001"; --     **
		when others => triotazniky <= "11111111";
	end case;
end process;

zobraz: process(row_cnt) --tuto to bude blikat
begin
	ROW <= row_cnt;
	if switch = '1' then
		LED <= triotazniky;
	else LED <= "11111111";
	end if;
end process;

end architecture chovanie;	


















