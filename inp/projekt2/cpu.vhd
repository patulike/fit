 -- cpu.vhd: Simple 8-bit CPU (BrainFuck interpreter)
-- Copyright (C) 2013 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): Zdenek Vasicek <vasicek AT fit.vutbr.cz>
-- Patricia Jamriskova, xjamri01

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
 port (
   CLK   : in std_logic;  
   RESET : in std_logic;  
   EN    : in std_logic; 
 
   -- synchronni pamet ROM
   CODE_ADDR : out std_logic_vector(11 downto 0); 
   CODE_DATA : in std_logic_vector(7 downto 0);   
   CODE_EN   : out std_logic;                     
   
   -- synchronni pamet RAM
   DATA_ADDR  : out std_logic_vector(9 downto 0); 
   DATA_WDATA : out std_logic_vector(7 downto 0); 
   DATA_RDATA : in std_logic_vector(7 downto 0); 
   DATA_RDWR  : out std_logic;                    
   DATA_EN    : out std_logic;                    
   
   -- vstupni port
   IN_DATA   : in std_logic_vector(7 downto 0);   
   IN_VLD    : in std_logic;                      
   IN_REQ    : out std_logic;                   
   
   -- vystupni port
   OUT_DATA : out  std_logic_vector(7 downto 0);  
   OUT_BUSY : in std_logic;                       
   OUT_WE   : out std_logic                      
 );
end cpu;


-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is
    
	type instruction_type is ( plusVal, minusValue, plusPtr, minusPtr, WhileStart, WhileEnd, JUMP, CPU_STOP, putValue, getValue);
	signal REPEAT : instruction_type;
	type fsm_state is (inicializacia, prinesenie, odkodovanie, ziadnaOperacia, zostavanieCPU, iDekrementaciaHodnoty, iInkrementaciaHodnoty,
   iDostanZnak, iWhileNacitavanie, iDajZnak, iWhileSTART, iWhileJUMP, iWhileEND, iWhilejumpEND);
	
  
    --Programovy citac
	signal PC : std_logic_vector(11 downto 0); 
	signal PC_inc : std_logic; 
  signal PC_dekrement : std_logic;
	signal RAS_to_PC : std_logic_vector(11 downto 0) :="000000000000"; 
	signal PC_load : std_logic := '0';
  
  
  	--Counter
	signal Counter_Hodnota : std_logic_vector(4 downto 0) :="00000"; 
	signal Counter_Inkrementacia : std_logic;
	signal Counter_Dekrementacia : std_logic; 
	signal Counter_InicializaciaHodnoty : std_logic; 
	signal Counter_NulaNula : std_logic; 
	
  
    --Pointer
	signal Pointer_Inkrementacia : std_logic; 
	signal Pointer_Dekrementacia : std_logic;  
	signal Pointer_Hodnota : std_logic_vector(9 downto 0); 
	
  
		--Mux
	signal predajMUX : std_logic_vector(1 downto 0); 
	signal DATA_RDATA_zero: std_logic; 
 
  
	  --RAS
	signal PUSH : std_logic; 
	signal POP : std_logic; 
	type t_stack is array (0 to 15) of std_logic_vector (11 downto 0); 
	signal STACK : t_stack; 
	
  	
    --FSM
	signal nstate : fsm_state;
	signal pstate : fsm_state;

	
  -- ---------------------------------------------------------------------
  --                          BEGIN
  -- ---------------------------------------------------------------------
	begin
  
    -- ---------------------------------------------------------------------
  --                          dalsi
  -- ---------------------------------------------------------------------
  
  
next_state_logic : process( RESET , CLK )
	
  begin
  
		if (RESET = '1') 
    then
			pstate <= prinesenie;
		elsif(CLK'event and CLK = '1') 
    then
			if (EN = '1') then
				pstate <= nstate;
			end if;
		end if;
	end process;
    -- ---------------------------------------------------------------------
  --                          PC
  -- ---------------------------------------------------------------------
	
  PROC : process (RESET, CLK, RAS_to_PC)
	
  begin
	       if (RESET = '1') 
          then
		       PC <= (others =>'0');
		     elsif (CLK'event and CLK = '1')
          then
         if (PC_inc = '1' and PC_dekrement = '0')
          then 
			    PC <= PC + 1;
			   elsif (PC_inc = '0' and PC_dekrement = '1')
          then 
			    PC <= PC-1;
			   elsif (PC_load = '1')
          then
				PC <= RAS_to_PC; 
			   end if;
		    end if;
	       end process;
	       CODE_ADDR <= PC;
	
	  -- ---------------------------------------------------------------------
  --                          RAS
  -- ---------------------------------------------------------------------
  
	RAS : process(RESET, CLK, POP, PUSH, PC)
  
	begin
  
	       	if (CLK'event and CLK = '1')
           then
			    if (PUSH = '1' and POP = '0')
           then
				STACK <= PC & STACK(0 to 14);
			   elsif (PUSH = '0' and POP = '1')
           then
				STACK <= STACK(1 to 15) & "000000000000";	
			   end if;
		      end if;
end process;
	
	RAS_to_PC <= STACK(0); 
  
	
	  -- ---------------------------------------------------------------------
  --                          POINTER
  -- ---------------------------------------------------------------------


	PRT : process (CLK, RESET, Pointer_Inkrementacia, Pointer_Dekrementacia, Pointer_Hodnota)
	
  begin
		
    if (RESET = '1')
     then
			Pointer_Hodnota <= "0000000000";
		elsif(CLK'event and CLK = '1')
     then
			if (Pointer_Inkrementacia = '1' and Pointer_Dekrementacia = '0')
       then
				Pointer_Hodnota <= Pointer_Hodnota +1;
			elsif (Pointer_Inkrementacia = '0' and Pointer_Dekrementacia = '1')
       then
				Pointer_Hodnota <= Pointer_Hodnota - 1;
			end if;
		end if;
	end process;
	DATA_ADDR <= Pointer_Hodnota;
  
    -- ---------------------------------------------------------------------
  --                          MUX
  -- ---------------------------------------------------------------------
	
	
	
	MUX_plus_zero : process(IN_DATA, DATA_RDATA, predajMUX)
	
  begin
	
  	case predajMUX is
			when "10" => DATA_WDATA <= DATA_RDATA + 1;
			when "01" => DATA_WDATA <= DATA_RDATA - 1;
			when others => DATA_WDATA <= IN_DATA;
		end case;
	end process;
	
	with DATA_RDATA select
		DATA_RDATA_zero <= '1' when "00000000",
							'0' when others;
              
  -- ---------------------------------------------------------------------
  --                          COUNTER
  -- ---------------------------------------------------------------------
              
              

	CNT:process(RESET, CLK, Counter_Inkrementacia, Counter_Dekrementacia, Counter_Hodnota, Counter_InicializaciaHodnoty)

	begin
  
	   	if (RESET = '1')
        then
			Counter_Hodnota <= "00000";
		  elsif( CLK'event and CLK = '1')
       then 
			if (Counter_Inkrementacia = '1' and Counter_Dekrementacia = '0')
       then
			Counter_Hodnota <= Counter_Hodnota + 1;
			elsif (Counter_Inkrementacia = '0' and Counter_Dekrementacia = '1')
       then
			Counter_Hodnota <= Counter_Hodnota - 1;
			elsif (Counter_InicializaciaHodnoty = '1')
       then
			Counter_Hodnota <= "00001";
		  	end if;
		  end if;
	   end process;
	with Counter_Hodnota select
		Counter_NulaNula <= '1' when "00000",
					'0' when others;
	

	  -- ---------------------------------------------------------------------
  --                          DECODER
  -- ---------------------------------------------------------------------
	
  
	decoder : process ( CODE_DATA )
	
  begin
	
    	case (CODE_DATA) is
			when X"3E" => REPEAT <= plusPtr;
			when X"3C" => REPEAT <= minusPtr;
			when X"2B" => REPEAT <= plusVal;
			when X"2D" => REPEAT <= minusValue;
			when X"5B" => REPEAT <= WhileStart;
			when X"5D" => REPEAT <= WhileEnd;
			when X"2E" => REPEAT <= putValue;
			when X"2C" => REPEAT <= getValue;
			when X"00" => REPEAT <= CPU_STOP;
			when others => REPEAT <= JUMP;
		  end case;
	end process;
	
  -- ---------------------------------------------------------------------
  --                          FSM
  -- ---------------------------------------------------------------------	

	
	-- FSM
	fsm : process(pstate, REPEAT, OUT_BUSY, DATA_RDATA, IN_VLD, DATA_RDATA_zero, Counter_NulaNula)
	
  begin
  
		PC_load <= '0';
		PC_dekrement <= '0';
		PC_inc <= '0';
		PUSH <= '0';
		POP <= '0';
		Counter_Inkrementacia <= '0';
		Counter_Dekrementacia <= '0';
		Pointer_Inkrementacia <= '0';
		Pointer_Dekrementacia <= '0';
		DATA_RDWR <= '1'; 
		DATA_EN <= '0';
		IN_REQ <= '0';
		DATA_EN <= '0';
		predajMUX <= "00";
    OUT_WE <= '0';
		Counter_InicializaciaHodnoty <= '0';
		OUT_DATA <= "00000000";
		CODE_EN <= '1';
    
		                      
		case pstate is
			when inicializacia =>
			CODE_EN <= '0';
				nstate <= prinesenie;
        
				
			when prinesenie => 
				CODE_EN <= '1';
				nstate <= odkodovanie;
        
			
			when odkodovanie =>
			case REPEAT is
				when plusVal =>
					DATA_EN <= '1'; 
					nstate <= iInkrementaciaHodnoty;
					when minusValue =>
					DATA_EN <= '1'; 
					nstate <= iDekrementaciaHodnoty;
					when plusPtr =>
					Pointer_Inkrementacia <= '1';
					PC_inc <= '1'; 
					nstate <= inicializacia;
					when minusPtr =>
					Pointer_Dekrementacia <= '1';
					PC_inc <= '1'; 
					nstate <= inicializacia;
					when JUMP =>
					PC_inc <= '1'; 
					nstate <= inicializacia;
					when putValue =>
					DATA_EN <= '1'; 
					nstate <= iDostanZnak;
					when getValue =>
					IN_REQ <= '1';
					nstate <= iDajZnak;
					when CPU_STOP =>
					nstate <= zostavanieCPU;
					when WhileStart =>
					DATA_EN <= '1'; 
					nstate <= iWhileSTART;
					when WhileEnd =>
					DATA_EN <= '1'; 
					nstate <= iWhileEND;
					when others =>
					nstate <= ziadnaOperacia;
					end case;
          
  -- ---------------------------------------------------------------------
  --                          CYKLY
  -- ---------------------------------------------------------------------          
			
      
			when iInkrementaciaHodnoty =>
				DATA_RDWR <= '0'; 
				DATA_EN <= '1'; 
				predajMUX <= "10"; 
				PC_inc <= '1'; 
				nstate <= inicializacia;
				
        
			when iDekrementaciaHodnoty =>
				DATA_RDWR <= '0'; 
				DATA_EN <= '1'; 
				predajMUX <= "01"; 
				PC_inc <= '1'; 
				nstate <= inicializacia;
			
      
			when iDostanZnak =>
				if ( OUT_BUSY = '1') then
					nstate <= iDostanZnak;
				else
					OUT_DATA <= DATA_RDATA;
					OUT_WE <= '1';
					PC_inc <= '1'; 
					nstate <= inicializacia;
				end if;
			
      
			when iDajZnak =>
				if (IN_VLD = '0') then
					nstate <= iDajZnak;
					IN_REQ <= '1';
				else
					IN_REQ <= '0';
					DATA_RDWR <= '0'; 
					DATA_EN <= '1'; 
					predajMUX <= "00"; 
					PC_inc <= '1'; 
					nstate <= inicializacia;
				end if;
				
        
			when iWhileSTART =>
				PC_inc <= '1'; 
				if( DATA_RDATA_zero = '1' ) then 
					Counter_InicializaciaHodnoty <= '1';
					nstate <= iWhilejumpEND;
				else
					PUSH <= '1' ; 
					nstate <= inicializacia;
				end if;
				
        
			when iWhilejumpEND =>
				nstate <= iWhileJUMP;
			
      
			when iWhileJUMP =>
				if ( Counter_NulaNula = '0' ) then
					if (REPEAT = WhileEnd) then
						Counter_Dekrementacia <= '1';
					elsif (REPEAT = WhileStart) then
						Counter_Inkrementacia <= '1';
					end if;
					PC_inc <= '1'; 
					nstate <= iWhilejumpEND;
				else
				nstate <= inicializacia;
				end if;
					
				
			when iWhileEND =>
				if( DATA_RDATA_zero = '1' ) then 
				PC_inc <= '1'; 
				POP <= '1';
				nstate <= inicializacia;
				else
				PC_load <= '1';
				nstate <= iWhileNacitavanie;
				end if;
				
			when iWhileNacitavanie =>
			POP <= '1';
			nstate <= inicializacia;
					
			when ziadnaOperacia =>
				nstate <= inicializacia;
			when others =>
			
		nstate <= zostavanieCPU;
  end case;
  end process;
  end behavioral;
  
    -- ---------------------------------------------------------------------
  --                          END cpu.vhd
  -- ---------------------------------------------------------------------
