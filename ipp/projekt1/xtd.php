<?php

#XTD:xjamri01

// vypis napovedy
function help (){
	echo "Nápověda:
		Parametry:
		--help - vypíše nápovědu
		--input=filename - zadaný vstupný soubor ve formátu XML
		--output=filename - zadaný vstupný soubor ve formátu definovaného výše
		--heather='hlavička' na začátek výstupního souboru se vloží zakomentovaná hlavička
		--etc=n pro n ≥ 0 určuje maximální počet sloupců vzniklých ze stejnojmenných podelementů
		-a nebudou se generovat sloupce z atributů ve vstupním XML souboru
		-b pokud bude element obsahovat více podelementů stejného názvu, bude se uvažovat, jako
by zde byl pouze jediný takový(tento parametr nesmí být kombinován s parametrem --etc=n)
		-g - výstupní soubor XML. Lze jej uplatnit v kombinaci s jakýmikoliv jinými přepínači vyjma --help;
	";
}


/**spracovani parametru****/
$shortopts = "a";
$shortopts .= "b";
$shortopts .= "g";

$options = getopt($shortopts, array(
	"help",
	"input:",
	"output:",
	"header:",
	"etc:",
    "isvalid:"
));

// zkontrolovat kolize a hodnoty parametru
if (count($options) != $argc - 1 || (isset($options['help']) && $argc > 2) || (isset($options['etc']) && $options['etc'] < 0) || (isset($options['b']) && isset($options['etc']))) {
	error(1, "Chybne parametry.");
}

if (isset($options["help"])) {      // pokud je help tak zobrzait napovedu a skoncit
	help();
	exit(0);
}

if (isset($options["input"])){
	if (($inputF = @fopen($options["input"], "r")) === false) {      // pokud je citelny tak otevrit
		error(2, "Vstupni soubor nelze otevrit.");
	}
}
else $inputF = @fopen("php://stdin", "r");


if (isset($options["output"])){
	if (($outputF = @fopen($options["output"], "w")) === false) {    // pokud je zapisovatelny tak otevrit
		error(3, "Vystupni soubor nelze zapisovat.");
	}
}
else $outputF = @fopen("php://stdout", "w");

// nacist cele xml
$input = "";
while (feof($inputF) === false) {
	$input .= fgets($inputF);
}

if (isset($options["isvalid"])) {
    if (($inputFV = @fopen($options["isvalid"], "r")) === false) {
        error(2, "IsValid soubor nelze otevrit.");
    }

    $inputV = "";
    while (feof($inputFV) === false) {
        $inputV .= fgets($inputFV);
    }
    @fclose($inputFV);

    $isvalid = run($inputV);        // spustit s isvalid souborem
    $inputOriginal = run($input);   // spustit s input souborem

    if ($isvalid == $inputOriginal) {   // poud jsou stejne tak se daji data vlozit
        clean($inputOriginal);
    } else {                            // jinak se nedaji vlozit => vratit kod 91
        error(91, "Ruzne xml.");
    }
} else {    // neni zadan --isvalid parametr
    clean(run($input));
}

/*** nacist xml, zpracovat dle parametru ***/
function run ($input) {
    global $options;

    /*** nacist a parsovat xml ***/
    $xml = @simplexml_load_string($input);

    if ($xml === false) {   // pokud se nepovedlo nacist xml
        error(4, "Chybny vstupni xml");
    }

    ob_start(); // odchytavat echo do bufferu

    $relations = array ();

    $tables = getXML($xml, $relations);
    $tables1 = toSingleArray($tables);

    if (isset($options["etc"])) {   // z book1_id, book2_id predelat na book_id a dat do dane tabulky
        $tables1 = filterEtc($tables1, $options["etc"]);
    }

    if (isset($options["header"])) {    // vypsat header
        echo "--" . $options["header"] . PHP_EOL . PHP_EOL;
    }

    if (isset($options["g"])) {
        relations($relations, $tables1);

        echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
        echo '<tables>' . PHP_EOL;

        foreach ($tables1 as $name => $table) {
            echo '<table name="' . $name . '">' . PHP_EOL;
            foreach ($tables1 as $name2 => $table2) {
                $relation = relation($relations, $name, $name2);
                if ($relation) {
                    echo '<relation to="' . $name2 . '" relation_type="' . $relation . '" />' . PHP_EOL;
                }
            }
            echo '</table>' . PHP_EOL;
        }

        echo '</tables>' . PHP_EOL;
    } else {
        /***vytvoreni SQL tabulky na vystup***/
        foreach ($tables1 as $name => $table) {
            echo "CREATE TABLE " . $name . "(" . PHP_EOL;

            $j = 0;
            $array_length = count($table);
            // projit kazdou tabulku
            foreach ($table as $col_name => $col_type) {
                if (is_int($col_type)) {        // pokud je hodnota cislo => pocet vyskytu daneho elementu
                    if (isset($options["b"])) { // pokud -b tak pocet vsech elementu je 1
                        $col_type = 1;
                    }

                    for ($i = 0; $i < $col_type; $i++) {    // book1_id, book2_id, ...
                        echo "\t" . $col_name . ($col_type > 1 ? $i + 1 : "") . "_id INT" . ($j == $array_length - 1 && $i == $col_type - 1 ? "" : ",") . PHP_EOL;
                    }
                } else {    // vypsat sloupec + typ (pokud nema typ empty a neni to value)
                    if ($col_name != "value" || $col_type != "EMPTY") {
                        $col_name = $col_name[0] == '@' ? substr($col_name, 1) : $col_name;
                        echo "\t" . $col_name . " " . $col_type . ($j == $array_length - 1 ? "" : ",") . PHP_EOL;
                    }
                }
                $j++;
            }
            echo ");" . PHP_EOL . PHP_EOL;
        }
    }

    return ob_get_clean();
}

/*** kontrola jednotlivych urovni (otec a potomek), vytvori pole podobne strukture XML ***/
function getXML ($xml, &$relations) {
    global $options;

    $output = array();

    foreach ($xml as $child) {
        $child_name = strtolower($child->getName());
        if (!isset($output[$child_name])) {
            $output[$child_name] = array();
        }

        if (!isset($relations[$child_name])) {  // pridat element do pole vztahu
            $relations[$child_name] = array();
        }

        if ($child->count()) {
            $output[$child_name][] = getXML($child, $relations);    // rekurzivne pridat vsechny potomky do pole
        } else {
            /***pridavam datove typy***/
            $value = strtolower(trim((string)$child));
            if (empty($value)) {                         // nema obsah
                $output[$child_name][]["TYPE"] = "EMPTY";
            } else if (empty($value) || $value == "true" || $value == "false" || $value == "0" || $value == "1") { // bit
                $output[$child_name][]["TYPE"] = "BIT";
            } else if ($value == (string)(int)$value) { // int
                $output[$child_name][]["TYPE"] = "INT";
            } else if (is_numeric($value)) {            // float
                $output[$child_name][]["TYPE"] = "FLOAT";
            } else {                                    // ntext
                $output[$child_name][]["TYPE"] = "NTEXT";
            }
        }

        // pokud neni nastaven parametr -a tak pridat atributy jako sloupce
        if (!isset($options["a"]) && $attrs = $child->attributes()) {
            foreach ($attrs as $key => $attr) {
                $key = strtolower($key);    // "@" pro odliseni atributu a sloupcu

                if ($key != "value") {      // value nemusi byt jen atribut
                    $key = '@' . $key;

                    if (strlen($key) > 3 + 1 && substr($key, -3, 3) == '_id' && isset($output[$child_name][0][substr($key, 1, -3)])) {
                        error(90, "Atribut je shodny s existujicim podelementem.");
                    }
                }

                $value = strtolower(trim((string)$attr));   // odstranit bile znaky a prevest na mala pismena

                if (!isset($output[$child_name][$key]["TYPE"])) {       // okud nema jeste typ
                    $output[$child_name][$key] = array ("TYPE" => "");  // vlozit nejnize prioritni typ
                }
                $old_type = $output[$child_name][$key]["TYPE"];

                if (empty($value) || $value == "true" || $value == "false" || $value == "0" || $value == "1") { // bit
                    $output[$child_name][$key]["TYPE"] = changeType($old_type, "BIT");
                } else if ($value == (string)(int)$value) {                 // int
                    $output[$child_name][$key]["TYPE"] = changeType($old_type, "INT");
                } else if (is_numeric($value)) {                            // float
                    $output[$child_name][$key]["TYPE"] = changeType($old_type, "FLOAT");
                } else {                                                    // nvarchar
                    $output[$child_name][$key]["TYPE"] = changeType($old_type, "NVARCHAR");
                }
            }
        }
    }

    return $output;
}

/*** prevede vystup z funkce getXML na jednoduche pole obsahujici sloupce, datove typy ***/
function toSingleArray(array $tables) {
    $tables1 = array();

    foreach ($tables as $name => $table) {
        if (!isset($table[0])) {    // pokud nema potomky tak jit na dalsi tabulku
            continue;
        }

        if (!isset($tables1[$name])) {
            // kazda tabulka ma primarni klic => rovnou pridat
            $tables1[$name] = array('prk_' . $name . '_id' => 'INT PRIMARY KEY');
        }

        if (is_array($table)) {
            foreach ($table as $k => $l) {
                if (is_int($k)) {            // prohledat potomky
                    $count = array ();       // pocitadlo podelementu jednotlivych urovni

                    if (is_array($l)) {
                        foreach ($l as $name_child => $child) {

                            if (is_array($child)) {
                                // spocitat vsechny elementy
                                foreach ($child as $i => $j) {
                                    if (!isset($count[$name_child])) {
                                        $count[$name_child] = 1;
                                        continue;
                                    }
                                    if (!is_int($i)) {
                                        continue;
                                    }

                                    $count[$name_child]++;
                                }
                                $tables1_2 = toSingleArray($l);             // rekurzi preskladat potomky
                                super_array_merge($tables1, $tables1_2);    // sloucit pole
                            }
                        }

                        // zjistit maximalni pocet podelementu v elementu
                        foreach ($count as $col_name => $col_count) {
                            if (!isset($tables1[$name][$col_name]) || $tables1[$name][$col_name] < $col_count) {
                                $tables1[$name][$col_name] = $col_count;
                            }
                        }
                    }
                } else {    // aktualne neni prohledavan potomek (treba atribut)
                    // nastavit typ
                    if (!isset($tables1[$name][$k])) {
                        $tables1[$name][$k] = $l["TYPE"];
                    } else {
                        $tables1[$name][$k] = changeType($tables1[$name][$k], $l["TYPE"]);
                    }
                }
            }
        }

        // nastavit typ podle priority
        if (isset($table[0]["TYPE"])) {
            $type = $table[0]["TYPE"];
            foreach ($table as $kk => $ch) {
                if (!is_int($kk) || !isset($ch["TYPE"])) {
                    continue;
                }

                $type = $tables1[$name]["value"] = changeType($type, $ch["TYPE"]);
            }
        }

        foreach ($table as $n => $t) {
            if ($n != "TYPE" && !is_int($n)) {
                if (!isset($tables1[$name][$n])) {
                    $tables1[$name][$n] = $t["TYPE"];
                } else {
                    $tables1[$name][$n] = changeType($tables1[$name][$n], $t["TYPE"]);
                }
            }
        }

    }

    return $tables1;
}

/*** protřídí pole pokud je zadán parametr --etc=n ***/
function filterEtc ($tables1, $etc) {
    $output = $tables1;

    foreach ($tables1 as $name => $table) {
        foreach ($table as $col_name => $col_type) {
            if (is_int($col_type) && $col_type > $etc) {
                unset($output[$name][$col_name]);
                $output[$col_name][$name . "_id"] = "INT";
            }
        }
    }

    return $output;
}

/** vygenerovat vztahy mezi tabulkami ***/
function relations(&$relations, $tables1) {
    foreach ($tables1 as $a => $table) {
        foreach ($tables1 as $b => $table2) {
            if ($a == $b) {                                     // sam se sebou
                $relations[$a][$b] = '1:1';
            } else if (isset($table[$b]) && isset($table2[$a])) {   // pokud existuje a->b && b->a
                $relations[$a][$b] = $relations[$b][$a] = 'N:M';
            } else if (isset($table[$b . '_id']) || (isset($table[$b]) && $table[$b] == 1)) {   // pokud je vyskyt 1 nebo cizí klic
                $relations[$a][$b] = 'N:1';
                $relations[$b][$a] = '1:N';
            } else if (isset($table[$b]) && $table[$b] > 1) {   // pokud je vyskyt elementu $b v elementu $a > 1
                $relations[$a][$b] = 'N:1';
                $relations[$b][$a] = '1:N';
            }
        }
    }

    // tranzitivita
    foreach ($tables1 as $a => $table) {                // projit vsechny tabulky
        foreach ($tables1 as $b => $table2) {           // k nim zkusit priradit nejakou z tabulek
            if (!isset($relations[$a][$b])) {           // pokud mezi sebou zatim nemaji zadny vztah
                foreach ($tables1 as $c => $table3) {   // projit vsechny tabulky a dat do nich stejny vztah
                    if (isset($relations[$a][$c]) && isset($relations[$c][$b])) {   // pokud existuje a->c && c->b
                        if ($relations[$a][$c] == $relations[$c][$b]) {             // 1:N & 1:N => 1:N, N:1 & N:1 => N:1
                            $relations[$a][$b] = $relations[$a][$c];
                    } else if (!isset($relations[$a][$b])) {                        // 1:N & N:1 || N:1 & 1:N => N:M
                            $relations[$a][$b] = 'N:M';
                        }
                    }
                }
            }
        }
    }
}

/** urci vztah mezi dvemi elementy ***/
function relation ($relations, $from, $to) {
    if (isset($relations[$from][$to])) {
        return $relations[$from][$to];
    }

    return '';
}

/*** spoji dve pole ***/
function super_array_merge (&$array1, &$array2) {
    foreach ($array2 as $k => $v) { // z druheho pole do prvniho
        if ($k == "TYPE" && isset($array1[$k])) {
            $array1[$k] = changeType($v, $array1[$k]);
        }
        else if (!isset($array1[$k]) || (!is_array($array1[$k]) && $array1[$k] < $array2[$k])) { //v prvnim poli nenalezen nebo je mensi jak v druhem
            $array1[$k] = $v;       // prenest hodnotu
        }
        else if (is_array($v)) {         // pokud je hodnota pole tak rekurzivne spojit
            super_array_merge($array1[$k], $array2[$k]);
        }
    }
}

/*** podle priority vrati vysledny typ ***/
function changeType ($old_type, $new_type) {
    $types = array("EMPTY", "BIT", "INT", "FLOAT", "NVARCHAR", "NTEXT");

    $old_p = array_search($old_type, $types);
    $new_p = array_search($new_type, $types);

    if ($old_p > $new_p) {
        return $old_type;
    }

    return $new_type;
}

/*** uklizeci funkce ***/
function clean ($output = "") {
    global $inputF, $outputF;

    @fwrite($outputF, $output);

    @fclose($inputF);
    @fclose($outputF);
}

/*** zapsat chybu na stderr a skoncit s $code navratovym kodem ***/
function error($code, $msg){
    clean();    // vse zavrit a uklidit
    file_put_contents("php://stderr", $msg);
    exit($code);
}
