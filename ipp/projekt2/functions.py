#!/usr/bin/env python3

#SYN:xjamri01

import argparse
import re
import sys


# zpracovani parametru
def parameters():

    parser = argparse.ArgumentParser(description='SYN do IPP, xjamri01', add_help=False)

    parser.add_argument("--help",
                        dest="show_help",
                        action="store_true",
                        default=False,
    )
    parser.add_argument("--format",
                        dest="format_filename",
                        default=None,
    )
    parser.add_argument("--input",
                        dest="input_filename",
                        default=None,
    )
    parser.add_argument("--output",
                        dest="output_filename",
                        default=None,
    )
    parser.add_argument("--br",
                        dest="br",
                        action="store_true",
                        default=False,
    )

    try:
        args = parser.parse_args()
    except:
        sys.stderr.write("pouzita spatna kombinace parametru")
        exit(1)

    read_file = ''
    try:
        if args.input_filename:
            fi = open(args.input_filename, 'r')
            read_file = fi.read()
            fi.close()
        else:
            read_file = sys.stdin.read()
    except:
        sys.stderr.write("soubor se nepodarilo otevrit pro cteni")
        exit(2)

    fo = False
    try:
        if args.output_filename:
            fo = open(args.output_filename, 'w')
        else:
            fo = sys.stdout
    except:
        sys.stderr.write("soubor se nepodarilo otevrit pro zapis")
        exit(3)

    if args.show_help:
        if len(sys.argv) == 2:
            fo.write("Projekt do IPP - SYN v jazyku Python, xjamri01\n"
                  "*************************************************\n"
                  "--help ..Vytiskne nápovědu\n"
                  "--format=filename ..Určení formátovacího souboru.\n"
                  "--input=filename ..Určení vstupního souboru v kódování UTF-8.\n"
                  "--output=filename .. Určení výstupního souboru opět v kódování UTF-8 s naformátovaným vstupním textem.\n"
                  "--br .. Přidá element <br /> na konec každého řádku původního vstupního textu.\n")
            fo.close()
            exit(0)
        else:
            fo.close()
            sys.stderr.write("help s vice paramerty")
            exit(1)

    if args.output_filename:
        fo.close()

    format_file = ''
    try:
        if args.format_filename:
            f = open(args.format_filename, 'r')
            format_file = f.read()
            f.close()
    except:
        pass

    return args, read_file, format_file

# nacteni formatu
def formats(format_file):
    formats = []

    formatLines = format_file.splitlines()
    compiled = re.compile('^([^\t]+)\t+((?:[a-zA-Z:0-9]+(?:,[ \t]+)?)+)$')

    for format in formatLines:
        if format == "":
            continue

        lineRe = compiled.search(format)
        if lineRe is None:
            sys.stderr.write("NONE: spatny format formatovaciho souboru")
            exit(4)
        styles = lineRe.group(2).replace(' ', '').replace('\t', '')

        # kontrola spravnosti stylu, pri chybe ukonci program
        tags(styles)

        # pridat rv a styly bez mezer a tabulatoru
        formats.append([toPyRegex(lineRe.group(1)), styles])

    return formats


# prevede regularni vyraz z ipp do python verze
def toPyRegex(ippRegex):
    regex = ''
    percentage = False
    neg = ''

    # prochazeni celeho regularniho vyrazu a vytvareni python verze
    for i in range(0, len(ippRegex)):
        c = ippRegex[i]

        # kontrola spatnych kombinaci v rv: zacatek .|)?*+, konec: ()
        if (i == 0 and c in ".|)?*+") or (c in ".|!" and (len(ippRegex) == 1 or (ippRegex[i-1] in ".|!" and ippRegex[i-2] != "%"))) or (ippRegex[i-1] != "%" and i == len(ippRegex) - 1 and c in "|.!(") or (ippRegex[i] == '(' and i < len(ippRegex)-1 and ippRegex[i+1] == ')'):
            sys.stderr.write("ZAVORKY: spatny format formatovaciho souboru")
            exit(4)
        # NQS rozsireni
        elif (c in '*+' and i < len(ippRegex)-1 and ippRegex[i+1] in '*+') or (percentage and c in '*+' and ippRegex[i-1] in '*+'):
            # preskocit druhy znak NQS rozsireni
            if percentage:
                continue

            # nasledujici 2 znaky
            d = ippRegex[i:2]
            if d == '++':
                regex += '+'
            else:
                regex += '*'
            percentage = True
            continue
        # procento
        elif c == '%' or percentage:
            if not percentage:
                percentage = True
                continue
            else:
                percentage = False

            if c == 's':
                regex += '[' + neg + ' \f\n\t\r\v]'
            elif c == 'a':
                if neg == '':
                    regex += '.'
            elif c == 'd':
                regex += '[' + neg + '0-9]'
            elif c == 'l':
                regex += '[' + neg + 'a-z]'
            elif c == 'L':
                regex += '[' + neg + 'A-Z]'
            elif c == 'w':
                regex += '[' + neg + 'a-zA-Z]'
            elif c == 'W':
                regex += '[' + neg + 'a-zA-Z0-9]'
            elif c == 't':
                regex += '[' + neg + '\\t]'
            elif c == 'n':
                regex += '[' + neg + '\\n]'
            elif c in '.|!*+()%':
                regex += neg + '\\' + c
            else:
                sys.stderr.write("ELSE: spatny format formatovaciho souboru")
                exit(4)
            neg = ''
        elif c == '!':
            if neg == '^':
                sys.stderr.write("NEGNEG: spatny format formatovaciho souboru")
                exit(4)
            else:
                neg = '^'
        elif c == '.':
            if i == len(ippRegex)-1 or ippRegex[i-1] == '.':
                sys.stderr.write("TECKY: spatny format formatovaciho souboru")
                exit(4)
            else:
                continue
        elif c in '\\^[]{}$?':
            regex += '\\' + c
        elif neg == '^':
            regex += '[^' + c + ']'
            neg = ''
        else:
            regex += c

    try:
        re.compile(regex)
    except:
        exit(4)

    return regex


# vlozi startovaci a ukoncovaci tagy na danou pozici
def positions(formats, read_file):
    # alokace pole prazdnych retezcu
    positions = [''] * (len(read_file)+1)

    for regex, styles in formats:
        compiled = re.compile(regex, re.DOTALL | re.MULTILINE)
        # najit vsechny vyskyty a vytvorit iterator
        matches = compiled.finditer(read_file)

        for match in matches:
            if match.start() == match.end():
                continue

            # vlozit pocatecni tag
            positions[match.start()] += tags(styles, False)
            # vlozit koncovy tag
            positions[match.end()] = tags(styles, True) + positions[match.end()]

    return positions


# generuje tagy podle vsech stylu
def tags(styles, ending = False):
    tags = ''
    stylesEx = styles.split(',')

    for style in stylesEx:
        tag = ''

        if re.match('^size:[1-7]$', style) or re.match('^color:[0-9A-F]{6}$', style):
            tag = 'font'
            if not ending:
                if style[0] == 's':
                    tag += ' size=' + style[5]
                else:
                    tag += ' color=#' + style[6:]
        elif style in ['bold', 'italic', 'underline']:
            tag = style[0]
        elif style == 'teletype':
            tag = 'tt'
        else:
            sys.stderr.write('ELSE TAGS: spatny format formatovaciho souboru')
            exit(4)

        # je pozadovano ukonceni tagu
        if ending:
            tags = '</' + tag + '>' + tags
        else:
            tags += '<' + tag + '>'

    return tags

# formatuje vystup zadanymi tagy
def format(tags, read_file, br):
    formatted = ''

    # projit vstupni soubor znak po znaku
    for i in range(0, len(read_file)):
        # vlozi tagy do formatovaneho souboru
        formatted += tags[i] + read_file[i]

    # vlozi tagy na konec
    formatted += tags[len(read_file)]

    # nahradit nove radky <br />
    if br:
        formatted = formatted.replace('\n', '<br />\n')

    return formatted

