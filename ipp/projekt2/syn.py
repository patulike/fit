#!/usr/bin/env python3

#SYN:xjamri01

import sys

import functions

args, read_file, format_file = functions.parameters()

formats = functions.formats(format_file)

if len(formats) == 0:
    if args.br:
        read_file = read_file.replace('\n', '<br />\n')
    if args.output_filename:
        fw = open(args.output_filename, 'w')
        fw.write(read_file)
        fw.close()
    else:
        sys.stdout.write(read_file)
    exit(0)

positions = functions.positions(formats, read_file)

formatted = functions.format(positions, read_file, args.br)

if args.output_filename:
    fo = open(args.output_filename, 'w')
    fo.write(formatted)
    fo.close()
else:
    sys.stdout.write(formatted)
