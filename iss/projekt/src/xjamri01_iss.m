%%%%%%%%%%%%%%%%%projekt_do_ISS%%%%%%%%%%%%%%%%%%%%%%%
%%%%xjamri01%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%zaostrenie obrazu%

iptsetpref('UseIPPL', false);
ORIGINALpicture = imread('xjamri01.bmp');
NEWpicture = imread('xjamri01.bmp');
ENDpicture = 'step1.bmp'

matrixFOCUS = [ -0.5 -0.5 -0.5; -0.5 5.0 -0.5; -0.5 -0.5 -0.5 ];
FOCUSpicture = imfilter(NEWpicture, matrixFOCUS);
imwrite(FOCUSpicture,ENDpicture);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%otocenie obrazu%

ENDpicture = 'step2.bmp'
rotatePICTURE = fliplr(FOCUSpicture);
imwrite(rotatePICTURE, ENDpicture); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%medianovy filter%%

ENDpicture = 'step3.bmp'
MEDIANpicture = medfilt2(rotatePICTURE, [5 5]);
imwrite(MEDIANpicture, ENDpicture);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%rozmazanie obrazu%%

ENDpicture = 'step4.bmp'
matrixNONfocus = [1 1 1 1 1; 1 3 3 3 1; 1 3 9 3 1; 1 3 3 3 1; 1 1 1 1 1]
nonFOCUSpicture = imfilter(MEDIANpicture, matrixNONfocus);
imwrite(nonFOCUSpicture, ENDpicture);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%chyba v obraze%%

 CMPpicture = fliplr(nonFOCUSpicture);
CMPpictureDOUBLE = im2double(CMPpicture);
ORIGpictureDOUBLE = im2double(ORIGINALpicture);
error = 0;
SIZEpicture = size(ORIGINALpicture);
SIZEpiXture = min(SIZEpicture);
SIZEpiYture = max(SIZEpicture);

for (x=1:SIZEpiXture)
for (y=1:SIZEpiYture)
error = error + abs(ORIGpictureDOUBLE(x,y) - CMPpictureDOUBLE(x,y));
end;
end;

ERRORtada = (error /(SIZEpiXture + SIZEpiYture))*255

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%roztiahnutie histogramu%%

histogram = im2double(nonFOCUSpicture);
ENDpicture = 'step5.bmp';
minimum = min(histogram);
start = min(minimum);
maximum = max(histogram);
startT = max(maximum);
ending =0.0;
endingG=1.0;
HISTOGRAMpicture = imadjust(nonFOCUSpicture, [start startT], [ending, endingG]);
imwrite(HISTOGRAMpicture, ENDpicture);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%stredna hodnota a smerodatna odchylka%%

 histogram = im2double(nonFOCUSpicture);
middleWITHOUThist=mean2(histogram)*255
odchylka=std2(histogram)*255
expand=im2double(HISTOGRAMpicture);
middleWITHhist = mean2(expand)*255
odchylka2=std2(expand)*255


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%kvantizacia obrazu%%

 a = 0;
b = 255;
N = 2;
ENDpicture = 'step6.bmp';
kvantization=zeros(SIZEpiXture, SIZEpiYture);
helpful=double(histogram);
for (x=1:SIZEpiXture)
for (y=1:SIZEpiYture)
kvantization(x,y)=round(((2^N)-1)*(helpful(x, y)-a)/(b-a))*(b-a)/((2^N)-1) + a;
end;
end;
kvant2=uint8(kvantization);
imwrite(kvant2, ENDpicture);

%%%%%%%%%%%%%%%%%%%%%%%%%%%koniec_suboru%%%%%%%%%%%%%%%%





