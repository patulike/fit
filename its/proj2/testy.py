# -*- coding: utf-8 -*-
#2.projekt ITS, Patricia Jamriskova, xjamri01@stud.fit.vutbr.cz

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class Testy(unittest.TestCase):
    def setUp(self):
        #self.driver = webdriver.Firefox()
        self.driver = webdriver.Remote(
                    command_executor='http://mys01.fit.vutbr.cz:4444/wd/hub',       # uri selenium server 
                    desired_capabilities=DesiredCapabilities.FIREFOX)
        self.driver.implicitly_wait(30)
        self.base_url = "https://ipa.demo1.freeipa.org/ipa/ui/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_admin(self):
        driver = self.driver
        driver.get(self.base_url)
        driver.find_element_by_id("username1").clear()
        driver.find_element_by_id("username1").send_keys("admin")
        driver.find_element_by_id("password2").clear()
        driver.find_element_by_id("password2").send_keys("Secret123")
        driver.find_element_by_css_selector("button[name=\"login\"]").click()
        for i in range(15):
            try:
                if "Active users" == driver.find_element_by_css_selector("div.facet-title.no-pkey").text: break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "Identity"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "Policy"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "Authentication"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "Network Services"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "IPA Server"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        driver.find_element_by_css_selector("span.loggedinas").click()
        driver.find_element_by_link_text("Logout").click()

    def test_bad_login(self):
        driver = self.driver
        driver.get(self.base_url)
        driver.find_element_by_id("username1").clear()
        driver.find_element_by_id("username1").send_keys("XYZ")
        driver.find_element_by_id("password2").clear()
        driver.find_element_by_id("password2").send_keys("12345678")
        driver.find_element_by_css_selector("button[name=\"login\"]").click()
        for i in range(15):
            try:
                if self.is_element_present(By.CSS_SELECTOR, "span.fa.fa-exclamation-circle"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        try: self.assertEqual("The password or username you entered is incorrect.", driver.find_element_by_xpath("//div[@id='simple-container']/div/div/div[2]/div/div/div/div[2]/div/div/div[2]/div/div/div").text)
        except AssertionError as e: self.verificationErrors.append(str(e))

    def test_manager(self):
        driver = self.driver
        driver.get(self.base_url)
        driver.find_element_by_id("username1").clear()
        driver.find_element_by_id("username1").send_keys("manager")
        driver.find_element_by_id("password2").clear()
        driver.find_element_by_id("password2").send_keys("Secret123")
        driver.find_element_by_css_selector("button[name=\"login\"]").click()
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "Users"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "OTP Tokens"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        try: self.assertEqual("Identity Settings", driver.find_element_by_css_selector("h2[name=\"identity\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Account Settings", driver.find_element_by_css_selector("h2[name=\"account\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Contact Settings", driver.find_element_by_css_selector("h2[name=\"contact\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Mailing Address", driver.find_element_by_css_selector("h2[name=\"mailing\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Employee Information", driver.find_element_by_css_selector("h2[name=\"employee\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Misc. Information", driver.find_element_by_css_selector("h2[name=\"misc\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("span.loggedinas").click()
        driver.find_element_by_link_text("Logout").click()

    def test_helpdesk(self):
        driver = self.driver
        driver.get(self.base_url)
        driver.find_element_by_id("username1").clear()
        driver.find_element_by_id("username1").send_keys("helpdesk")
        driver.find_element_by_id("password2").clear()
        driver.find_element_by_id("password2").send_keys("Secret123")
        driver.find_element_by_css_selector("button[name=\"login\"]").click()
        for i in range(15):
            try:
                if "Active users" == driver.find_element_by_css_selector("div.facet-title.no-pkey").text: break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "Identity"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "Policy"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "Authentication"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "Network Services"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "IPA Server"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        driver.find_element_by_css_selector("span.loggedinas").click()
        driver.find_element_by_link_text("Logout").click()

    def test_employee(self):
        driver = self.driver
        driver.get(self.base_url)
        driver.find_element_by_id("username1").clear()
        driver.find_element_by_id("username1").send_keys("employee")
        driver.find_element_by_id("password2").clear()
        driver.find_element_by_id("password2").send_keys("Secret123")
        driver.find_element_by_css_selector("button[name=\"login\"]").click()
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "Users"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for i in range(15):
            try:
                if self.is_element_present(By.LINK_TEXT, "OTP Tokens"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        try: self.assertEqual("Identity Settings", driver.find_element_by_css_selector("h2[name=\"identity\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Account Settings", driver.find_element_by_css_selector("h2[name=\"account\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Contact Settings", driver.find_element_by_css_selector("h2[name=\"contact\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Mailing Address", driver.find_element_by_css_selector("h2[name=\"mailing\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Employee Information", driver.find_element_by_css_selector("h2[name=\"employee\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Misc. Information", driver.find_element_by_css_selector("h2[name=\"misc\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("span.loggedinas").click()
        driver.find_element_by_link_text("Logout").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
