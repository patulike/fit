#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>

/*
1.PROJEKT do IZP: Pocitanie slov
Patricia Jamriskova, xjamri01@fit.vutbr.cz */



//deklaracia funkcii
int help();
int read_array (char cs, int index, int debug);


//chybove stavy
enum ERR_err
{
    ERR_malo_parametrov,
    ERR_spatne_parametry,
    ERR_ina_chyba,
};

const char *ERR_msg[] =
{
    "Zadal si malo parametrov. Pozri sa do napovedy --help\n",

    "Tvoje parametry su prekvapivo zle. Pozri si napovedu --help\n",

    "Nastala neocakavana chyba, utec od pocitaca a uz ho nechytaj\n",
};


void print_err(int errkod)
{


    if (errkod < ERR_malo_parametrov || errkod > ERR_ina_chyba)
    {
        errkod = ERR_ina_chyba;
    }

    fprintf(stderr, "%s", ERR_msg[errkod]);
}


//hlavne telo programu
int main(int argc, char *argv[])
{
    char c = 0;
    int index = -1;
    int debug = 0;

    if (argc < 2) {
        print_err(ERR_malo_parametrov);
        return 1;
    }
    if (argc > 4) {
    print_err(ERR_spatne_parametry);
    return 1;
    }

    if ((strcmp(argv[1], "--help") == 0))
    {
        help();
        return 0;
    }

    c = argv[1][0];

    if (argc == 3 )
    {
        if ((strcmp(argv[2], "-d") == 0))
        {
            debug = 1;
        }
        else
        {
            for (int i = 0; argv[2][i] !=0; ++i)
            {
                if ((argv[2][i] <= '9') && (argv[2][i] >= '0' ))
                {
                    index *= 10;
                    index = (int)argv[2][i] - 48;
                }
                else {break;}
            }
            if (index < 0){
                index = -index;
            }
        }
    }
    else if (argc == 4)
    {
        if ((strcmp(argv[3], "-d") == 0))
        {
            debug = 1;
        }
        else {
            print_err(ERR_spatne_parametry);
            return 1;
        }
        for (int i = 0; argv[2][i] !=0; ++i)
        {
            if ((argv[2][i] <= '9') && (argv[2][i] >= '0' ))
            {
                index *= 10;
                index = (int)argv[2][i] - 48;
            }
            else {
                print_err((ERR_ina_chyba));
                return 1;
            }
        }
        if (index < 0){
            index = -index;
        }
    }
    //printf("%c, %d, %d =>\n",c, index, debug); //c vyjadruje zadany znak, index je druhy parameter->ak neni zadany tak je jeho hodnota -1; debug zapina debugovaci rezim
    read_array(c, index, debug);
    return 0;
}



int read_array (char cs, int index, int debug) //funkcia precita vstup aka "pole" a pracuje s parametrami
{
    int c;
    int count = 0;
    int found = 0;
    int word_idx = 0;

    while ((c = getchar()) != EOF) //cita vstup
    {
        word_idx++;
        if (debug == 1 && (c == '-' || c == '_' || (c >= 'a' && c <= 'z') || (c >= 'A' && c<= 'Z') || (c >= '0' && c<= '9') )){
            printf("%c", c);
        } else if (debug == 1 && (c == '\n' || c == ' ')) {
            printf("\n");
        }
        if (index != -1){
            if (c != ' ' && c != '\n' && word_idx < index){
                continue;
            }
        }
        if (cs == ':'){ //cs su specialne znaky
            if ((c >= '0' && c <= '9') && found == 0){
                count++;
                found = 1;
            } else if (c == ' ' || c == '\n'){
                found = 0;
                word_idx = 0;
            }
        } else if (cs == '^'){
            if ((c >= 'A' && c <= 'Z') && found == 0){
                count++;
                found = 1;
            } else if (c == ' ' || c == '\n'){
                found = 0;
                word_idx = 0;
            }
        } else if (cs == '.'){
            if (found == 0 && c != ' ' && c != '\n'){
                count++;
                found = 1;
            } else if (c == ' ' || c == '\n'){
                found = 0;
                word_idx = 0;
            }
        } else {
            if (c == cs && found == 0){
                count++;
                found = 1;
            } else if (c == ' ' || c == '\n'){
                found = 0;
                word_idx = 0;
            }
        }
    }
    printf("%d\n", count); //count je pocet slov
    return 0;
}



int help()
{

 {

   char *napoveda =
    "\n*********************************************\n"
    "Projekt c.1 do IZP: POCITANI SLOV           *\n"
    "(c)Patricia Jamriskova, xjamri01, 2BIA      *\n"

    "\nSpustenie:                                  *\n"

        " ./proj1 --help                             *\n"
        " ./proj1 X [N] [-d]                         *\n"

    "\n*Legenda*                                   *\n"
    " --help   --> napoveda                      *\n"
    "    X     --> hladany znak                  *\n"

    "\n**Volitelne**                               *\n"
    "   [N]    --> cislo pozicie hladaneho znaku *\n"
    "  [-d]    --> zapina debug                  *\n"
    "*********************************************\n";

    printf("%s\n", napoveda);
 }
    return 0;
}


