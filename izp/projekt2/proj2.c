
/* ***************************proj2.c****************************************** */
/* **************************************************************************** */
/* *******                  ITERACNE VYPOCTY                        *********** */
/* Patricia Jamriskova**********************************[xjamri01@fit.vutbr.cz] */
/* **************************************************************************** */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


const double IZP_PI_2 = 1.57079632679489661923;    //definovana konstanta pi/2

double my_sqrt(double x); //funkcia sqrt ako odmocnina
double my_asin(double x); // funkcia asin ako arcsinus
int triangle(double a1, double a2, double b1, double b2, double c1, double c2); //funkcia triangle ako pocitanie uhlov trojuholnikov

//chybove stavy
enum ERR_err
{
    ERR_malo_spatne_param,
    ERR_ina_chyba,
};


const char *ERR_msg[] =
{
    "Zadal si bud malo alebo vela parametrov.\n",


    "Poriadne si rozmysli, co pises ---> ./proj2 --help\n",
};


void print_err(int errkod)
{
    if (errkod<ERR_malo_spatne_param || errkod>ERR_ina_chyba)
    {
        errkod = ERR_ina_chyba;
    }
    fprintf(stderr, "%s", ERR_msg[errkod]);
}


void napoveda()
{
printf("\n"
       "2.Projekt do IZP: ITERACNE VYPOCTY\n"
       "(c) Patricia Jamriskova [xjamri01@fit.vutbr.cz]\n"
       "\n"
       "***************************************************\n"
       "FUNKCIE:\n"
            "   --sqrt X [vypocita druhu odmocninu z cisla]\n"
            "   --asin X [vypocita arcus sinus z cisla]\n"
            "   --triangle AX AY BX BY CX CY [vypocita a vypise 3 uhly v trojuholniku]\n"
            "   --help [vypise napovedu]\n"
            "\n   ");
}

//nacitanie parametrov z terminalu
int main(int argc,char *argv[])
{
    if ((argc < 2) || ((argc >3) && (argc <8)) || (argc > 9)) //pozeram kolko parametrov je zadanych, zly pocet je definovany chybou a vytlaci napovedu
    {
        print_err(ERR_malo_spatne_param);
        print_err(ERR_ina_chyba);
        return 1;
    }


     if(argc==2)
    {
        if((strcmp(argv[1], "--help") == 0))
        {
            napoveda(); //zavolam napovedu
        }
    }
    if (argc==3)
    {
        if ((strcmp(argv[1], "--sqrt") == 0)) //zapinam funkciu druhej odmocniny
        {
            double x = atof(argv[2]); //do argv[2] priradujem cislo X, ktore zadavam z terminalu
            printf("%.10e\n", my_sqrt(x)); //vypise hodnotu druhej odmocniny z funkcie
        }
        else if (strcmp(argv[1], "--asin")== 0) //zapinam funkciu arcus sinus
        {
            double x = atof(argv[2]); //do argv[2] priradujem cislo X, ktore zadavam z terminalu
            printf("%.10e\n", my_asin(x)); //vypise hodnotu arcsinu z funkcie

        }
    }
    else if (argc == 8 && strcmp(argv[1], "--triangle")== 0) //zapinam funkciu vypoctu uhlov trojuholnikov
    {
        triangle(atof(argv[2]), atof(argv[3]), atof(argv[4]), atof(argv[5]), atof(argv[6]), atof(argv[7]));
        //do argv[i+1] priradujem 6 cisel z terminalu
    }
    return 0;
}



double my_sqrt(double x) //funkcia na vypocet druhej odmocniny
{
    double dalsie = 1;
    double predchozi = 0;

    if ((x) < 0.0)
    {
        return 0.0/0.0; //vracia -nan
    }

    if (x == 0.0)
    {
        return 0.0;
    }

    while(!(dalsie - predchozi <= 0.00000000001 && dalsie - predchozi >= -0.00000000001)) //podmienka na absolutnu hodnotu, 0.00000000001 je definovany eps
    {
        predchozi = dalsie;
        dalsie = 0.5 * (x/dalsie + dalsie);
    }
    return dalsie; //vratim povodnu hodnotu
}


double my_asin(double x) //funkcia na vypocet arcsinu v rozpati -1 az 1
{
    if (x == 1){
        return IZP_PI_2; //definovane zo vzorca
    } else if (x > 1){
        return 0.0/0.0;
    } else if (x == -1){
        return -IZP_PI_2;
    } else if (x < -1){
        return 0.0/0.0;
    }
    double i = 3;
    double dalsie = x;
    double vysl = 0;
    double constant = 0.5; //definovane zo vzorca
    while(!(vysl - (vysl - dalsie) <= 0.00000000001 && vysl - (vysl - dalsie) >= -0.00000000001))

    {
        dalsie *= (x * x);

        vysl += (dalsie * constant / i);

        constant *= i /(i+1);
        i += 2;

    }
    vysl += x;
    return vysl;
}


int triangle(double a1, double a2, double b1, double b2, double c1, double c2) //funkcia na vypocet uhlov trojuholnikov
{

    if ((a1==0) && (a2==0) && (b1==0) && (b2==0) && (c1==0) && (c2==0)) //ak su vsetky suradnice 0
    {
        printf("nan\n");
        printf("nan\n");
        printf("nan\n");

        return 10; //vracam 10 lebo to nie je vo funkcnom obore
    }

    if ((a1=b1=c1) && (a2=b2=c2)) //ak sa suradnice rovnaju same sebe
    {
        printf("nan\nnan\nnan\n");
        return 10;
    }

    double a_len = my_sqrt((b1 - c1) * (b1 - c1) + (b2 - c2) * (b2 - c2)); //dlzka a
    double b_len = my_sqrt((a1 - c1) * (a1 - c1) + (a2 - c2) * (a2 - c2)); //dlzka b
    double c_len = my_sqrt((b1 - a1) * (b1 - a1) + (b2 - a2) * (b2 - a2)); //dlzka c



    double alfa, beta, gama;



    alfa = IZP_PI_2 - my_asin(((-a_len) * a_len + b_len * b_len + c_len * c_len) / (2*b_len*c_len));
    beta = IZP_PI_2 - my_asin((a_len * a_len - b_len * b_len + c_len * c_len) / (2*a_len*c_len));
    gama = IZP_PI_2 - my_asin((a_len * a_len + b_len * b_len - c_len * c_len) / (2*a_len*b_len));


    if (((alfa<=0) && (beta<=0)) && (gama<=0))
    {
        printf("nan\nnan\nnan\n");
        return 10;
    }


    else if ((alfa<=0) && (gama<=0))
    {
        printf("nan\n%.10e\nnan\n", beta);
        return 10;
    }

    else if ((alfa<=0) && (beta<=0))
    {
        printf("nan\nnan\n%.10e\n", gama);
        return 10;
    }

    else if ((beta<=0) && (gama<=0))
    {
        printf("%.10e\nnan\nnan\n", alfa);
        return 10;
    }



    printf("%.10e\n%.10e\n%.10e\n", alfa, beta, gama);

    return 0;





}

/* *********************************konec proj2.c ******************************************** */










